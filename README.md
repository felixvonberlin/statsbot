[![Python](https://img.shields.io/badge/using-Python%203-blue.svg?logo=python)](https://www.python.org)
[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![discord.py](https://img.shields.io/badge/using-discord.py-00bb88.svg?logo=discord&logoWidth=20)](https://github.com/Rapptz/discord.py)

# StatsBot
This a simple bot for discord which allows you to track the online time on discord of the members.

## I want to run my own instance
In order to run your own instance, you need to install python3 on your maschine.

It's also essential, that the following python packages are installed.

 * discord.py
 * sqlite3

You are able to install python packages like this: `pip3 install [package name]`

Now you need to create your own bot token at discordapp.com, and paste
 it into the quotation marks which you will find in the source code.

It's done; start the bot with `python3 bot.py`

# I need further assistance for running my own bot.
Please contact me via [e-mail](mailto:statsbot@von-oertzen-berlin.de).
