import sqlite3
import os
import time
import datetime


#    StatsBot
#    Copyright (C) 2019  Felix v. O.
#    felix@von-oertzen-berlin.de
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

class User:
  def __init__(self, userID, name, optoutState, callback):
    self.__id = userID
    self.__name = name
    self.__optoutState = optoutState
    self.__callback = callback
    
  def getName(self):
    return self.__name
  
  def getID(self):
    return self.__id
  
  def setName(self, name):
    self.__name = name
    if self.__callback != None:
      self.__callback(self)
  
  def setCallback(self, cb):
    self.__callback = cb
  
  def isOptedOut(self):
    return self.__optoutState
  
  def setOptOutStatus(self, state):
    self.__optoutState = state
    if self.__callback != None:
      self.__callback(self)

class DatabaseManager:
  def __init__(self, db):
    self.__users = []
    self.__guilds = {}
    self.__connection = sqlite3.connect(db)
    if db != "memory" and os.path.getsize(db)< 100:
      self.__createDB()
    self.__loadUsers()
  def __createDB(self):  
    sql_user_table = """
      CREATE TABLE USER (
      user INTEGER PRIMARY KEY,
      name CHAR(64) NOT NULL,
      optout BOOLEAN NOT NULL);"""
      
    sql_time_table = """
      CREATE TABLE TIME (
      entry INTEGER PRIMARY KEY,
      user INTEGER NOT NULL,
      event CHAR(10) NOT NULL,
      time DATETIME NOT NULL);
      """
    sql_guilds_table = """
      CREATE TABLE TOGETHERNESS (
      user INTEGER PRIMARY KEY,
      guild INTEGER NOT NULL);"""
    
    cursor = self.__connection.cursor()  
    cursor.execute(sql_guilds_table)
    cursor.execute(sql_time_table)
    cursor.execute(sql_user_table)
    self.__connection.commit()
    cursor.close()
  
  def __callback(self, user):
    self.__updateUser(user)
    
  def __updateUser(self, user):
    sql = """UPDATE USER SET name = ?, optout = ? WHERE user = ?"""
    cursor = self.__connection.cursor()
    cursor.execute(sql, [user.getName(), user.isOptedOut(), user.getID()])
    self.__connection.commit()
    cursor.close()

  def __loadUsers(self):
    sql_usrs = """SELECT user, name, optout FROM USER"""
    sql_glds = """SELECT guild, user FROM TOGETHERNESS"""
    
    cursor = self.__connection.cursor()
    cursor.execute(sql_usrs)
    data = cursor.fetchall()
    self.__users = []
    for user in data:
      self.__users.append(User(int(user[0]), user[1], bool(int(user[2])), self.__callback))
    self.__guilds = {}
    cursor.execute(sql_glds)
    data = cursor.fetchall()
    cursor.close()
    for guild in data:
      if int(guild[0]) in self.__guilds.keys():
        self.__guilds[int(guild[0])].append(int(guild[1]))
      else:
        self.__guilds.update({int(guild[0]):[int(guild[1])]})

  def getUsers(self):
    return self.__users
  
  def getUserIDs(self):
    return [u.getID() for u in self.__users]
    
  def getUserFor(self, ID):
    for usr in self.__users:
      if usr.getID() == ID:
        return usr;
  
  def getGuilds(self):
    return self.__guilds
  
  def getGuildIDs(self):
    return list(self.__guilds.keys())
    
  def getMembersOf(self, guildID):
    return [self.getUserFor(x) for x in self.__guilds[guildID]]
    
  def addUser(self, user):
    user.setCallback(self.__callback)
    self.__users.append(user)
    sql = """INSERT INTO USER (user,name, optout) VALUES (?,?,?)"""
    cursor = self.__connection.cursor()
    cursor.execute(sql, [user.getID(), user.getName(), user.isOptedOut()])
    self.__connection.commit()
    cursor.close()
  
  def addGuild(self, guildID):
    self.__guilds.update({guildID:[]})
  
  def addUserToGuild(self, user, guild):
    self.__guilds[guild].append(user)
    sql = """INSERT INTO TOGETHERNESS (user, guild) VALUES (?,?)"""
    cursor = self.__connection.cursor()
    cursor.execute(sql, [user, guild])
    self.__connection.commit()
    cursor.close()
    
  def deleteUserinGuild(self, userID, guildID):
    doubleMember = False
    for guild in self.__guilds.keys():
      if guildID != guild:
        doubleMember = True
    cursor = self.__connection.cursor()
    if doubleMember:
      self.__guilds[guildID].remove(userID)
    else:
      sql_del_tms = """DELTE FROM TIME WHERE user = ?"""
      sql_del_usr = """DELETE FROM USER WHERE user = ?"""
      cursor.execute(sql_del_tms)
      cursor.execute(sql_del_usr)
    cursor.execute("DELETE FROM TOGETHERNESS WHERE user = ? and guild = ?", [userID, guildID])
    self.__connection.commit()
    
  def deleteGuild(self, guildID):
    usrs = self.__guilds[guildID]
    for mem in self.__guilds[guildID]:
      self.deleteUserinGuild(mem, guildID)
    del self.__guilds[guildID]
  
  def insertTimeLine(self, userID, evt):
    cursor = self.__connection.cursor()
    query = """INSERT INTO TIME (user, event, time) VALUES (?,?,?);"""
    #query = query.format(user=userID, event=evt, t=time.strftime("%Y-%m-%d %T", time.gmtime())) 
    cursor.execute(query, [userID, evt, time.strftime("%Y-%m-%d %T", time.gmtime())])    
    self.__connection.commit()
    cursor.close()

  def changeOptOut(self, userID, opt):
    for user in self.__users:
      if user.getID() == userID:
        user.setOptOutStatus(opt)

  def changeName(self, userID, newName):
    for user in self.__users:
      if user.getID() == userID:
        user.setName(newName)

  def getTimeLine(self, usr, since):
    sql_get_data = """SELECT event, time FROM TIME WHERE user = ? """ 
    sql_time_addon = """AND time >= ?"""
    cursor = self.__connection.cursor()
    sql_get_data = sql_get_data + ("" if since == None else sql_time_addon)
    
    cursor.execute(sql_get_data, [usr] if since == None else [usr, (datetime.datetime.utcnow() - since).strftime("%Y-%m-%d %T")])
    return cursor.fetchall()

