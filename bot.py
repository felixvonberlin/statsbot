import discord
import time
import datetime
import sys
import calendar 
import DatabaseManager
import Utils
import StatisticsService

#    StatsBot
#    Copyright (C) 2019  Felix v. O.
#    felix@von-oertzen-berlin.de
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

class StatsClient(discord.Client):
  def __init__(self):
    super().__init__()
    self.__db = DatabaseManager.DatabaseManager("userDB.db")
      
  async def on_ready(self):
    print('Logged on as', self.user)
    game = discord.Game("with the API")
    await client.change_presence(status=discord.Status.online, activity=game)
    self.__checkForNewUsers()
    self.__updateTimeTable()

  def __checkForNewUsers(self):
    glds = self.__db.getGuildIDs()
    for gld in self.guilds:
      if not(gld.id in glds):
        self.__db.addGuild(gld.id)

    usrs = self.__db.getUserIDs()
    recAdded = [] # If there are 2 new servers with same people
    for guild in self.guilds:
      for member in guild.members:
        if not(member.id in usrs) and not(member.id in recAdded):
          self.__db.addUser(DatabaseManager.User(member.id, member.name, False, None))
          self.__db.addUserToGuild(member.id, guild.id)
          recAdded.append(member.id)

  def __updateTimeTable(self, guild = None):
    for guild in (self.guilds if guild == None else [guild]):
      for member in guild.members:
        self.__db.insertTimeLine(member.id, str(member.status))

  async def on_guild_join(self,guild):
    self.__checkForNewUsers()
    self.__updateTimeTable(guild)
    await guild.system_channel.send(
      """I'm your Bot for Statistics.
      I will log your online presence on discord.
      If you don't want me to track you, you're able to opt out; Just tell me.""")

  async def on_guild_remove(self, guild):
    self.__db.deleteGuild(guild.id)
 
  async def on_member_leave(self, member):
    self.__db.deleteUserinGuild(member.id, member.guild.id)
    if member.guild.system_channel != None and printmessage:
      await member.guild.system_channel.send(member.name + " left this server.\n Deleting all data...")
    
  async def on_member_join(self, member):
    if not(member.id in self.__db.getUserIDs()):
      self.__db.addUser(DatabaseManager.User(member.id, member.name, False))
    self.__db.addUserToGuild(member.id, member.guild.id)
    if member.guild.system_channel != None:
      await member.guild.system_channel.send("Hi " + member.name + "!\n" + 
          "I would like to collect data about your time on Discord.\n" +
          "If you want to opt out, I would be sad, but you're able to do so.\n" +
          "Just tell me.")  

  async def on_member_update(self, before, after):
    if after.name != before.name:
      self.__db.changeName(before.id, after.name)
      
    if after.status != before.status:
      if self.__db.getUserFor(after.id).isOptedOut():
        return;
      self.__db.insertTimeLine(after.id, str(after.status))
      
  async def on_message(self, message):
    text  = message.content.lower()
    words = text.split() 
    
    # don't respond to ourselves
    if message.author == self.user:
      return

    if (type(message.channel) == discord.DMChannel):                            
      time.sleep(0.5)                                                           
      if words[0] == "/showdata":                                               
        if message.author.id in self.__db.getUserIDs():                         
          end = ""                                                                 
          for data in self.__db.getTimeLine(message.author.id, None):              
            end = end + str(data) + "\n";                                       
          async with message.channel.typing():                                  
                await message.channel.send(end) 
      else:
        await message.channel.send(
          "Hi " + message.author.name + ",\n"
          "I'm not designed for direct communication.\n" + 
          "I also should not answer your questions here due \n" +
          "privacy reasons. No one is allowed to spy on someone else. Except me! xD\n" + 
          "But here we you're able to see your data in my head, which actually is a SQLite database.\n\n" + 
          "So, I'm able to show you everything, I know about you.\n" + 
          "If you want me to forget everything about you, you need to ask the server admin to kick me.\n" + 
          "===============\nAvailable Commands:\n" + 
          "/showdata")
      return
    
    #Group Channel
    if not("<@" + str(self.user.id) + ">" in text):
      return
    if "opt out" in text:
      if not(self.__db.getUserFor(message.author.id).isOptedOut()):
        self.__db.changeOptOut(message.author.id, True)
        await message.channel.send("You want me to ignore you. It's okay.")
      else:
        await message.channel.send("I ignore you already!!eleven!")
    if "opt in" in text:
      if self.__db.getUserFor(message.author.id).isOptedOut():
        self.__db.changeOptOut(message.author.id, False)
        await message.channel.send("I'm happy that we are friends again.") 
      else:
        await message.channel.send("You already agreed.")
    if "opt in?" in text or "opted in?" in text:
      await message.channel.send("You've opted " + ("out" if self.__db.getUserFor(message.author.id).isOptedOut() else "in") + ".");

    if "give me" in text:
     # try:
        if self.__db.getUserFor(message.author.id).isOptedOut():
          await message.channel.send("Oooops, <@" + str(message.author.id) + "> doesn't want me to track him/her.")
          return
        TIMES = [["last year" , datetime.timedelta(days  = 365)],
                 ["last month", datetime.timedelta(days  =  30)],
                 ["last week" , datetime.timedelta(days  =   7)],
                 ["last 24h"  , datetime.timedelta(hours =  24)],
                 ["last day"  , datetime.timedelta(hours =  24)],
                 ["last 12h"  , datetime.timedelta(hours =  12)],
                 ["last hour" , datetime.timedelta(hours =   1)]]
        hot_user  = words.index("user") if "user" in words else -1 
        hot_time  = words.index("since")
        uID       = 0
        allowed   = False
        timeDelta = None
        if words[hot_user + 1].isdigit():
          uID = int(words[hot_user + 1])
          for mem in self.__db.getMembersOf(message.channel.guild.id):
            if uID == mem:
              allowed = True
        elif "myself" in words:
          uID = message.author.id 
          allowed = True
        else:  
          for mem in self.__db.getMembersOf(message.channel.guild.id):
            if mem.getName().lower() == words[hot_user + 1]:
              uID = mem.getID()
              allowed = True
    
        if not(allowed):
          await message.channel.send("Oooops, you're trying to get information, you're not allowed to see.")
          return
        
        if uID == 0:
          await message.channel.send("It's not clear which data you want to have. Please only ask for a specific user at one time!")
          return
          
        for tt in TIMES:
          if tt[0] in text:
            timeDelta = tt[1]
        if timeDelta == None:
          await message.channel.send("It's not clear which data you want to have. Please specify a amount of time!")
          return
          
        data = StatisticsService.StatisticsService(self.__db).loadStatisticsFor(uID, timeDelta)
        await message.channel.send("So, let's take a look at <@" + str(uID) + ">:")
        for ds in data:
          await message.channel.send("`" + Utils.getBlockString(ds[0], 6) + "\t: " + Utils.getReadableTimeString(ds[1]) + "`");
      #except Exception as e:
       # print(e)
        #await message.channel.send("Something seems to be wrong. Please contact my dad. (" + str(e) + ")")

client = StatsClient()
client.run("***")
