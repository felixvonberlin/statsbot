#    StatsBot
#    Copyright (C) 2019  Felix v. O.
#    felix@von-oertzen-berlin.de
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

def getReadableTimeString(timeInS):
  TIMES = {60:"minute", 60*60:"hour", 60*60*24:"day", 60*60*24*7:"week", 60*60*24*7*4:"month", 60*60*24*7*4*52:"year"}
  seconds = list(TIMES.keys())[::-1]
  res = ""
    
  for sec in seconds:
    div = int(timeInS/sec)
    if div > 0:
      res = res + str(div) + " " + TIMES[sec] + ("s" if div > 1 else "") + " "
      timeInS = timeInS % sec
  return res if res != "" else "no time recorded"
  
def getBlockString(s, length):
  wh = " " * length
  return (s + wh)[:length]

